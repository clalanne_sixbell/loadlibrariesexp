

#include "itu_pre_proc.h"

#include <stdio.h>

static int x0;
static int x1;

int x3;

int itu_pre_proc() {
  printf("[itu_pre_proc]\n");

  extern int y1;

  x0++;
  x1++;
  x3++;

  printf("[itu_pre_proc] x0 addr[%p] x1 addr[%p] x3 addr[%p] y1 extern[%p]\n",
         &x0, &x1, &x3, &y1);

  return x0 + x1 + x3 + y1;
}