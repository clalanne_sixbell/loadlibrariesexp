
#include "g729enc.h"

#include "itu_pre_proc.h"

#include <stdio.h>

#include <dlfcn.h>

int y1 = 11;

int encode_init(struct G729enc* enc) {
  printf("[encode_init]\n");

  enc->id = 67;
  enc->state = ENC_IDLE;

  enc->handle = dlmopen(
      LM_ID_NEWLM, "/home/christian/Sixbell/loadlibrariesexp/libg729ITU.so",
      RTLD_NOW | RTLD_LOCAL);

  if (enc->handle == NULL) {
    printf("[encode_init] ERROR: loading ITU library\n");
    char* error;
    if ((error = dlerror()) != NULL) {
      fprintf(stderr, "%s\n", error);
    }
    return FAIL;
  }

  printf("[encode_init] handle[%p]\n", enc->handle);

  return SUCCESS;
}

int encode_frame(struct G729enc* enc) {
  printf("[encode_frame]\n");
  int ret = 0;

  ret = encode_handle_frame(enc);
  if (ret != SUCCESS) {
    return FAIL;
  }
  return SUCCESS;
}

int encode_handle_frame(struct G729enc* enc) {
  printf("[encode_handle_frame]\n");
  int res = 0;
  typedef int (*FuncType)();
  printf("[encode_handle_frame] handle[%p]\n", enc->handle);

  FuncType itu_pre_proc = (FuncType)dlsym(enc->handle, "itu_pre_proc");
  printf("[encode_handle_frame]1\n");
  res = (*itu_pre_proc)();
  printf("[encode_handle_frame]2\n");
  // res = itu_pre_proc();
  printf("preprocess res[%d]", res);

  /*code();
  post_proc();
  */

  return SUCCESS;
}

void encode_show(struct G729enc* enc) {
  printf("[encode_show] encode ID[%d]\n", enc->id);
  printf("[encode_show] encode STATE[%d]\n", enc->state);
}
