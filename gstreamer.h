
#define SUCCESS 0

#define STATE_IDLE 0
#define STATE_PLAYING 1

struct Gstreamer {
  int id;
  int state;
};

#ifdef __cplusplus
extern "C" {
#endif

int init(struct Gstreamer *gs);
void show(struct Gstreamer *gs);
int play(struct Gstreamer *gs);

#ifdef __cplusplus
}
#endif