
#define SUCCESS 0
#define FAIL -1

#define ENC_IDLE 0
#define ENC_ACTIVE 1

struct G729enc {
  int id;
  int state;
  void *handle;
};

int encode_init(struct G729enc *enc);
int encode_frame(struct G729enc *enc);
int encode_handle_frame(struct G729enc *enc);
void encode_show(struct G729enc *enc);