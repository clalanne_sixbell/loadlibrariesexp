# Overview

The idea is to study loading libraries, with the goal of using these techniques on the backend/gstreamer

# Compilation

## ITU reference code

```
 gcc -shared -o libg729ITU.so -fPIC itu_pre_proc.c
```

## Encoder

```-D_GNU_SOURCE``` to enable ```__USE_GNU```, which are the definitions on ```dlfcn.h```
for example ```LM_ID_NEWLM```

```
 gcc -shared -o libg729enc.so -fPIC g729enc.c -ldl -I/usr/include -D_GNU_SOURCE
```

## Gstreamer library simulator

### In 2 steps
```
gcc -c -fPIC gstreamer.c -o gstreamer.o
```

```
gcc gstreamer.o -shared -o libgstreamer.so
```

### In 1 step

```
 gcc -shared -o libgstreamer.so -fPIC gstreamer.c -ldl
```

## Backend simulator compilation

```
g++ backend.cpp -L. -lgstreamer -o backend -ldl
```
