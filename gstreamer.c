#include "gstreamer.h"

#include "g729enc.h"

#include <dlfcn.h>
#include <stdio.h>

int init(struct Gstreamer* gs) {
  gs->id = 5;

  return SUCCESS;
}

void show(struct Gstreamer* gs) {
  printf("[gstreamer][show] id[%d]\n", gs->id);
}

int play(struct Gstreamer* gs) {
  printf("[play]\n");
  gs->state = STATE_PLAYING;
  int ret = 0;

  void* handle;

  handle = dlopen("./libg729enc.so", RTLD_NOW | RTLD_LOCAL);
  printf("[play][%p]\n", handle);

  typedef int (*FuncType)(struct G729enc * enc);
  FuncType encode_frame = (FuncType)dlsym(handle, "encode_frame");
  FuncType encode_init = (FuncType)dlsym(handle, "encode_init");

  typedef void (*FuncType1)(struct G729enc * enc);
  FuncType1 encode_show = (FuncType1)dlsym(handle, "encode_show");

  /*********************************************************************************/
  /*********************************************************************************/
  /*********************************************************************************/
  struct G729enc enc1;
  ret = (*encode_init)(&enc1);
  if (ret != SUCCESS) {
    return FAIL;
  }

  ret = (*encode_frame)(&enc1);
  if (ret != SUCCESS) {
    return FAIL;
  }

  (*encode_show)(&enc1);
  /*********************************************************************************/
  /*********************************************************************************/
  /*********************************************************************************/
  struct G729enc enc2;
  ret = (*encode_init)(&enc2);
  if (ret != SUCCESS) {
    return FAIL;
  }

  ret = (*encode_frame)(&enc2);
  if (ret != SUCCESS) {
    return FAIL;
  }

  (*encode_show)(&enc2);
  /*********************************************************************************/
  /*********************************************************************************/
  /*********************************************************************************/

  return SUCCESS;
}