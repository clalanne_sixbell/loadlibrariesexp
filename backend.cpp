#include <iostream>

#include "gstreamer.h"

int main() {
  std::cout << "backend_server starting" << std::endl;
  Gstreamer gs;

  int ret = 0;

  ret = init(&gs);
  if (ret != SUCCESS) {
    std::cerr << "something went wrong initializing gstreamer" << std::endl;
    return -1;
  }

  show(&gs);

  ret = play(&gs);
  if (ret != SUCCESS) {
    std::cerr << "something went wrong playing" << std::endl;
    return -1;
  }

  std::cout << "backend_server went well ..." << std::endl;

  return SUCCESS;
}