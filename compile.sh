# ITU reference code
gcc -shared -o libg729ITU.so -fPIC itu_pre_proc.c

# Encoder
gcc -shared -o libg729enc.so -fPIC g729enc.c -ldl -I/usr/include -D_GNU_SOURCE

# Gstreamer mock
gcc -shared -o libgstreamer.so -fPIC gstreamer.c -ldl

# backend
g++ backend.cpp -L. -lgstreamer -o backend -ldl
